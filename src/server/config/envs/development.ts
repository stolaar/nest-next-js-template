export const config = {
  db: {
    type: process.env.DB_TYPE || 'sqlite',
    synchronize: false,
    logging: true,
    host: process.env.DB_HOST || '127.0.0.1',
    port: process.env.DB_PORT || 3306,
    username: process.env.DB_USER || 'username',
    password: process.env.DB_PASSWORD || 'password',
    database: process.env.DB_NAME || 'localdb/dbname',
    extra: {
      connectionLimit: 10,
    },
    path: 'localdb/',
    autoLoadEntities: true,
  },
  foo: 'dev-bar',
}
