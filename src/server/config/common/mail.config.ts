import { MailerOptions } from '@nestjs-modules/mailer/dist/interfaces/mailer-options.interface'

const config: Partial<MailerOptions> = {
  transport: {
    host: process.env.EMAIl_HOST,
    port: parseInt(process.env.EMAIL_PORT),
    ignoreTLS: true,
    secure: false,
    auth: {
      user: process.env.EMAIL_INCOMING_USER,
      pass: process.env.EMAIL_INCOMING_PASS,
    },
  },
  defaults: {
    from: process.env.EMAIL_FROM,
  },
}

export default config
