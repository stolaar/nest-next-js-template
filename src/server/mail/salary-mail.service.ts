import { Injectable } from '@nestjs/common'
import { MailerService } from '@nestjs-modules/mailer'

@Injectable()
export class SalaryMailService {
  constructor(private mailerService: MailerService) {}

  sendSalariesSubmittedEmail(to: string, context: { name: string }) {
    return this.mailerService.sendMail({
      to,
      template: '/salary/salaries-submitted',
      context,
    })
  }
}
