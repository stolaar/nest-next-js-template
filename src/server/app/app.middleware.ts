import type { INestApplication } from '@nestjs/common'
import compression from 'compression'
import helmet from 'helmet'
import appConstants from '../../shared/utils/constants/app.constants'

export function middleware(app: INestApplication): INestApplication {
  app.use(compression())

  // https://github.com/graphql/graphql-playground/issues/1283#issuecomment-703631091
  // https://github.com/graphql/graphql-playground/issues/1283#issuecomment-1012913186
  app.use(
    helmet({
      contentSecurityPolicy: appConstants.isDev ? false : undefined,
      crossOriginEmbedderPolicy: appConstants.isDev ? false : undefined,
    }),
  )
  // app.enableCors();

  return app
}
