import {
  MiddlewareConsumer,
  Module,
  NestModule,
  Scope,
  ValidationPipe,
  DynamicModule,
} from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { RenderModule } from 'nest-next'
import Next from 'next'
import appConstants from '../../shared/utils/constants/app.constants'
import { CommonModule, HttpExceptionFilter, LoggerMiddleware } from '../common'
import { APP_FILTER, APP_PIPE } from '@nestjs/core'
import { BaseModule } from '../base/base.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { configuration } from '../config'

declare const module: any

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    }),
    CommonModule,
    BaseModule,
    TypeOrmModule.forRootAsync({
      useFactory: async (config: ConfigService) => ({
        ...(await config.get('db')),
      }),
      inject: [ConfigService],
    }),
    RenderModule.forRootAsync(Next({ dev: appConstants.isDev }), {
      viewsDir: null,
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    // Global Guard, Authentication check on all routers
    // { provide: APP_GUARD, useClass: AuthenticatedGuard },
    // Global Filter, Exception check
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
      scope: Scope.REQUEST,
    },
    // Global Pipe, Validation check
    // https://docs.nestjs.com/pipes#global-scoped-pipes
    // https://docs.nestjs.com/techniques/validation
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({
        // disableErrorMessages: true,
        transform: true, // transform object to DTO class
        whitelist: true,
      }),
    },
  ],
})
export class AppModule implements NestModule {
  public configure(consumer: MiddlewareConsumer): void {
    consumer.apply(LoggerMiddleware).forRoutes('*')
  }

  public static initialize(): DynamicModule {
    /* during initialization attempt pulling cached RenderModule
        from persisted data */
    const renderModule =
      module.hot?.data?.renderModule ??
      RenderModule.forRootAsync(Next({ dev: appConstants.isDev }), {
        viewsDir: null,
      })

    if (module.hot) {
      /* add a handler to cache RenderModule
          before disposing existing module */
      module.hot.dispose((data: any) => {
        data.renderModule = renderModule
      })
    }

    return {
      module: AppModule,
      imports: [renderModule],
      controllers: [AppController],
      providers: [AppService],
    }
  }
}
