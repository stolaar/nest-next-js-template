import { NestFactory } from '@nestjs/core'
import { AppModule } from './app/app.module'
import { middleware } from './app/app.middleware'
import { Logger } from './common'

declare const module: any

async function bootstrap() {
  const app = await NestFactory.create(AppModule.initialize(), {
    bodyParser: true,
    bufferLogs: true,
  })
  app.useLogger(await app.resolve(Logger))

  middleware(app)
  await app.listen(3000)

  if (module.hot) {
    module.hot.accept()
    module.hot.dispose(() => app.close())
  }
}
bootstrap()
