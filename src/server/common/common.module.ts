import { Global, Module } from '@nestjs/common'

import { Logger, RequestContext } from './logger'
import * as providers from './providers'

const services = [Logger, RequestContext, ...Object.values(providers)]

@Global()
@Module({
  exports: services,
  providers: services,
})
export class CommonModule {}
